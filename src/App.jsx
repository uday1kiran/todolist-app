import { useState } from "react";
import TaskInput from "./components/TaskInput";

function App() {
  const [toDoList, setTodoList] = useState([]);

  const addTask = (taskName) => {
    const newTask = { taskName, checked: false };
    setTodoList([...toDoList,new])
  };
  return (
    <div className="container">
      <h1>Task Master</h1>
      <TaskInput addTask={addTask}/>
      <div className="toDoList">
        <span>To do</span>
        <ul className="list-items"></ul>
      </div>
    </div>
  );
}

export default App;
