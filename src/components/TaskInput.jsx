import React, { useState } from "react";

const TaskInput = (addTask) => {
  const [task, setTask] = useState("");

  function handleInputValue(event) {
    setTask(event.target.value);
  }

  function handleAddTask() {
    addTask(task);
  }

  return (
    <form className="inputField">
      <input type="text" placeholder="Add Item" onChange={handleInputValue} />
      <button>+</button>
    </form>
  );
};

export default TaskInput;
